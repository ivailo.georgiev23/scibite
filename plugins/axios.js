export default function ({ $axios, redirect }) {
  const authToken = ''
  $axios.onRequest((config) => {
    $axios.setToken(authToken, 'Bearer')
  })
  $axios.onError((error) => {
    if (error.response.status === 500) {
      redirect('/sorry')
    }
  })
}
